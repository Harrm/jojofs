import unittest
import tempfile
from pathlib import Path
from shutil import rmtree

from storage_server.fsutils import FSUtils


class FSUtilsTests(unittest.TestCase):
    def setUp(self) -> None:
        self._tmp = Path(tempfile.mkdtemp())
        self._fs_util = FSUtils(self._tmp)

    def tearDown(self) -> None:
        rmtree(self._tmp)

    def test_touch(self):
        self._fs_util.touch('a/b/c.txt')
        self.assertTrue((self._tmp / 'a/b/c.txt').exists())
        self._fs_util.touch('a/b')
        self.assertTrue((self._tmp / 'a/b').is_dir())

    def test_init(self):
        self._fs_util.touch('a/b/c/d/e.txt')
        self._fs_util.touch('a/b/8/d/e.txt')
        self._fs_util.touch('a/b/0/d/e.txt')
        self._fs_util.touch('a/1/c/d/e.txt')
        self._fs_util.touch('a/1/c/d/5.txt')
        self._fs_util.touch('a/1/c/d/6.txt')
        self._fs_util.touch('a/1/c/d/7.txt')

        self._fs_util.init()

        self.assertTrue(self._tmp.exists())
        self.assertEqual(len(list(self._tmp.iterdir())), 0)

    def test_delete(self):
        self._fs_util.touch('a/b/c/d/e.txt')
        self._fs_util.touch('a/b/8/d/e.txt')
        self._fs_util.touch('a/b/0/d/e.txt')
        self._fs_util.touch('a/1/c/d/e.txt')
        self._fs_util.touch('a/1/c/d/5.txt')
        self._fs_util.touch('a/1/c/d/6.txt')


if __name__ == '__main__':
    unittest.main()
