import unittest
from xmlrpc.client import ServerProxy

from storage_server.fsutils import FSUtils
from storage_server.rpc_server import Server
from storage_server.mocks import NameNodeProxyMock, StorageServerProxyMock
from storage_server.storage import StorageSystem


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._call_address = "http://localhost:8080/"
        storage_system = StorageSystem(("localhost", 8080), FSUtils('/'), StorageServerProxyMock.factory)
        self._server = Server(("localhost", 8080), storage_system, NameNodeProxyMock())

    def test_ping(self):
        with ServerProxy(self._call_address) as proxy:
            res1 = proxy.ping()

        self.assertEqual(res1, None)

    def tearDown(self) -> None:
        self._server.down()


if __name__ == '__main__':
    unittest.main()
