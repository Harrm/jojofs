"""
Abstractions for rpc clients bindings
"""
from abc import abstractmethod
from typing import Tuple


class INameNodeProxy:
    """
        An abstraction for rpc client to call the namenode
    """

    @abstractmethod
    def file_created(self, file_name: str):
        pass

    @abstractmethod
    def join(self, hostname: str, port: int):
        pass


class IStorageServerProxy:
    """
        An abstraction for rpc client to call other storage servers
    """

    @abstractmethod
    def initiate_download(self, address: Tuple[str, int], where: str):
        pass


class AbstractStorageProxyFactory:
    """
        A factory that generates StorageServerProxy objects for specific addresses
    """

    @abstractmethod
    def create(self, address: str) -> IStorageServerProxy:
        pass
