"""
Module with rpc clients bindings
"""
from typing import Tuple
from xmlrpc.client import ServerProxy

from storage_server.rpc_clinets_abs import INameNodeProxy, IStorageServerProxy, AbstractStorageProxyFactory


class NameNodeProxy(INameNodeProxy):
    """
    An rpc client to call the namenode
    """

    def __init__(self, address: str):
        self._target_address = address

    def join(self, hostname: str, port: int):
        self._call_rpc("join", hostname, port)

    def file_created(self, file_name: str):
        self._call_rpc("received", file_name)

    def _call_rpc(self, name: str, *argv):
        with ServerProxy(self._target_address) as proxy:
            return proxy.__getattr__(name)(*argv)


class StorageServerProxy(IStorageServerProxy):
    """
        An explicit interface to communicate to others storage servers
    """
    class _Factory(AbstractStorageProxyFactory):
        def create(self, address: str) -> IStorageServerProxy:
            return StorageServerProxy(address)

    factory: AbstractStorageProxyFactory = _Factory()

    def __init__(self, target_address):
        self._target_address = target_address

    def initiate_download(self, address: Tuple[str, int], where: str):
        self._call_rpc("initiate_download", where)

    def _call_rpc(self, name: str, *argv):
        with ServerProxy(self._target_address) as proxy:
            return proxy.__getattr__(name)(*argv)
