"""
Utils for interacting with file system
"""
from shutil import copy, move, rmtree
from pathlib import Path
from typing import Union


class FSUtils:
    """
    Utils for interacting with file system
    """

    def __init__(self, base_dir):
        self._base_dir = Path(base_dir)

    def init(self):
        rmtree(self._base_dir)
        self._base_dir.mkdir()

    def move(self, src, dst):
        self._mkdir(dst)
        # noinspection PyTypeChecker
        move(self._actual_path(src), self._actual_path(dst))

    def copy(self, src, dst):
        self._mkdir(dst)
        # noinspection PyTypeChecker
        copy(self._actual_path(src), self._actual_path(dst))

    def touch(self, path):
        self._mkdir(path)
        self._actual_path(path).touch()

    def read(self, path):
        with self._actual_path(path).open('rb') as file:
            while True:
                data = file.read(1024)
                if not data:
                    break
                yield data

    def writer(self, path):
        self._mkdir(path)
        return self._actual_path(path).open('wb')

    def delete(self, path):
        """
        Delete either file or directory
        """
        actual = self._actual_path(path)
        if actual.is_dir():
            rmtree(actual)
        else:
            actual.unlink()

    def _mkdir(self, path):
        """
        call this to ensure existence of the folder
        """
        actual = self._actual_path(path)
        Path('/'.join(actual.parts[:-1])).mkdir(parents=True, exist_ok=True)

    def _actual_path(self, path: Union[str, Path]) -> Path:
        return self._base_dir / path
