"""
    Main unit of the StorageServer.
    Handles incoming rpc and reacts to them.
"""
from threading import Thread
from typing import Tuple
from xmlrpc.server import SimpleXMLRPCServer

from storage_server.storage import StorageSystem
from storage_server.rpc_clients import INameNodeProxy


class Server:
    """
    Main unit of the StorageServer.
    Handles incoming rpc and reacts to them.
    """

    def __init__(self, node_address: Tuple[str, int], storage_system: StorageSystem, namenode_proxy: INameNodeProxy):
        self._namenode_proxy = namenode_proxy

        self._server = SimpleXMLRPCServer(node_address, allow_none=True)
        self._server.register_function(lambda: None, "ping")
        self._server.register_instance(storage_system)

        Thread(target=self._server.serve_forever).start()
        print(f"Server started serving at {node_address}")

        self._namenode_proxy.join(node_address[0], node_address[1])
        print(f"I've connected to namenode")

    def down(self):
        self._server.shutdown()
