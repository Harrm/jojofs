"""
Mocks implementations for testing
"""
from typing import Tuple

from common import tcp_file_sharing
from storage_server.rpc_clients import INameNodeProxy
from storage_server.rpc_clinets_abs import IStorageServerProxy, AbstractStorageProxyFactory


class NameNodeProxyMock(INameNodeProxy):
    """
    Mock for the NameNodeProxyAbs
    """

    def join(self, hostname: str, port: int):
        print(f'I, {hostname}:{port}, have joined, yay')

    def file_created(self, file_name: str):
        print(f'file {file_name} created.')


class StorageServerProxyMock(IStorageServerProxy):
    """
        A mocks communication to other storage nodes
    """
    class _Factory(AbstractStorageProxyFactory):
        def create(self, address: str) -> IStorageServerProxy:
            return StorageServerProxyMock(address)

    factory: AbstractStorageProxyFactory = _Factory()

    def __init__(self, target: str):
        self._target = target

    def initiate_download(self, address: Tuple[str, int], where: str):
        downloaded = bytearray()
        tcp_file_sharing.client_downloader(address[0], address[1], downloaded.extend)
        print(f'@{self._target} Downloading file {where} from {address[0]}:{address[1]}')
        print(f'content: {downloaded}')
