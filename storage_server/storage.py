"""
A cross-storage manipulation utils.
"""
from typing import Tuple

from storage_server.fsutils import FSUtils
from storage_server.rpc_clinets_abs import AbstractStorageProxyFactory

from common import tcp_file_sharing

Node = Tuple[str, int]


class StorageSystem:
    """
    A class that implements RPCs
    """

    def __init__(self, current_address: Node, fs_utils: FSUtils, rpc_factory: AbstractStorageProxyFactory):
        self._current_address = current_address
        self._fs_utils = fs_utils
        self._storage_rpc_factory = rpc_factory

    def init(self):
        self._fs_utils.init()
        return 256

    def create(self, path: str):
        self._fs_utils.touch(path)

    def fetch(self, path: str) -> (str, int):
        file_iter = iter(self._fs_utils.read(path))
        host = self._current_address[0]
        port = tcp_file_sharing.server_uploader(host, file_iter)

        return host, port

    def put(self, path: str) -> (str, int):
        host = self._current_address[0]
        with self._fs_utils.writer(path) as writer:
            port = tcp_file_sharing.server_downloader(host, writer.write)

        return host, port

    def initiate_download(self, _from: Node, path: str):
        with self._fs_utils.writer(path) as writer:
            tcp_file_sharing.client_downloader(_from[0], _from[1], writer.write)

    def remove(self, path: str):
        self._fs_utils.delete(path)

    def copy(self, src: str, dst_address: Node, dst_path: str):
        # rpc changes type of dst_address from Tuple to List
        if tuple(dst_address) == self._current_address:
            if src == dst_path:
                raise RuntimeError(f"WTF, src and dst are the same...")
            self._fs_utils.copy(src, dst_path)
        else:
            self._copy_to_remote(src, dst_address, dst_path)

    def _copy_to_remote(self, src: str, dst_address: Node, dst_path: str):
        port = tcp_file_sharing.server_uploader(self._current_address[0], iter(self._fs_utils.read(src)))
        self._storage_rpc_factory.create(f'http://{dst_address[0]}:{dst_address[1]}') \
            .initiate_download(
                (self._current_address[0], port),
                dst_path
            )

    def move(self, src: str, dst: str):
        self._fs_utils.move(src, dst)

    def rmdir(self, path):
        self._fs_utils.delete(path)
