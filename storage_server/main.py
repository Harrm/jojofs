"""
An entry point to the Storage Server
"""

from argparse import ArgumentParser
import os

from storage_server.fsutils import FSUtils
from storage_server.mocks import NameNodeProxyMock
from storage_server.rpc_clients import NameNodeProxy, StorageServerProxy
from storage_server.rpc_server import Server
from storage_server.storage import StorageSystem


def main():
    parser = ArgumentParser()
    parser.add_argument('-s', '--storage_path', required=True, help='Path to the local storage.')
    parser.add_argument('-m', '--namenode_address', required=True, help='Address of the NameServer.')
    parser.add_argument('-a', '--node_address', required=True, help='Host associated with current node.')
    parser.add_argument('-p', '--node_port', required=True, help='The desired port to run on.')
    parser.add_argument('--mock_namenode', action='store_true')
    args = parser.parse_args()

    if not os.path.exists(args.storage_path):
        os.makedirs(args.storage_path, exist_ok=True)
    try:
        host = args.node_address
        port = int(args.node_port)
    except ValueError:
        print("Incorrect port: {}".format(args.node_port))
    else:
        if args.mock_namenode:
            namenode_proxy = NameNodeProxyMock()
        else:
            namenode_proxy = NameNodeProxy(args.namenode_address)
        storage_system = StorageSystem((host, port), FSUtils(args.storage_path), StorageServerProxy.factory)
        Server((host, port), storage_system, namenode_proxy)


if __name__ == '__main__':
    main()
