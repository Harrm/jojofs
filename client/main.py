"""
Now it's just to test naming server's RPCs
"""
import xmlrpc.client

with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:
    print("Storage for file: %s" % str(proxy.find_storage(3)))
