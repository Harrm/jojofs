"""
A wrapper for RPC ServerProxy with explicit API
"""
from pathlib import Path
from xmlrpc.client import ServerProxy, ProtocolError

from name_server.common import Location, FileInfo


class StorageServerProxy:
    """
    A wrapper for RPC ServerProxy with explicit API
    """
    address: Location

    def __init__(self, location: Location):
        self.address = location

    def replicate(self, file_path: Path, replica: Location):
        with ServerProxy(self.address) as rpc_proxy:
            return rpc_proxy.replicate(str(file_path), replica)

    def ping(self) -> bool:
        try:
            with ServerProxy(self.address) as rpc_proxy:
                rpc_proxy.ping()
            return True

        except ProtocolError:
            return False
