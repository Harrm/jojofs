import datetime
import random
import unittest
from pathlib import Path

from name_server.common import FileInfo, FileAttrs
from name_server.filesystem_meta_storage import FilesystemMetaStorage


class FilesystemMetaStorageTest(unittest.TestCase):

    def test_something(self):
        s = FilesystemMetaStorage()
        f1 = self._create_dir_info('etc')
        f2 = self._create_dir_info('a')
        f3 = self._create_dir_info('b')
        f4 = self._create_file_info('v')

        s.put(f1, Path('/'))
        s.put(f2, Path('/etc'))
        s.put(f3, Path('/etc/a'))
        s.put(f4, Path('/etc/a/b'))
        s.remove(Path('/etc/a/b/v'))
        self.assertRaises(FilesystemMetaStorage.DirectoryNotEmptyError, s.remove, Path('/etc/a'))
        s.remove(Path('/etc/a'), recursive=True)
        s.remove(Path('/etc'))
        self.assertRaises(FilesystemMetaStorage.RemovalOfRootError, s.remove, Path('/'))

    @staticmethod
    def _create_dir_info(name):
        rand_id = str(int(random.random() * 10000))
        return FileInfo(id=rand_id, name=name,
                        attrs=FileAttrs(creation_time=datetime.datetime.now(),
                                        modification_time=datetime.datetime.now(),
                                        is_dir=True))

    @staticmethod
    def _create_file_info(name):
        rand_id = str(int(random.random() * 10000))
        return FileInfo(id=rand_id, name=name,
                        attrs=FileAttrs(creation_time=datetime.datetime.now(),
                                        modification_time=datetime.datetime.now()))


if __name__ == '__main__':
    unittest.main()
