import unittest

from name_server.storage_server_set import StorageServerSet

class StorageServerSetTest(unittest.TestCase):
    def test_init(self):
        s = StorageServerSet()
        self.assertTrue(s)

    def test_add(self):
        s = StorageServerSet()
        s.add(("127.0.0.1", 42))
        s.add(("0.0.0.0", 34))
        self.assertTrue(s.contains(("127.0.0.1", 42)))
        self.assertTrue(s.contains(("0.0.0.0", 34)))

    def test_remove(self):
        s = StorageServerSet()
        s.add(("127.0.0.1", 42))
        s.add(("0.0.0.0", 34))
        s.remove(("0.0.0.0", 34))
        self.assertTrue(s.contains(("127.0.0.1", 42)))
        self.assertFalse(s.contains(("0.0.0.0", 34)))

    def test_get(self):
        s = StorageServerSet()
        ls = [("127.0.0.1", 42), ("0.0.0.0", 34)]
        s.add(("127.0.0.1", 42))
        s.add(("0.0.0.0", 34))
        for i in range(2):
            self.assertTrue(s.get(i).address in ls)
            self.assertEqual(s.get(i), s.get(i))
        self.assertNotEqual(s.get(0), s.get(1))

    def test_size(self):
        s = StorageServerSet()
        s.add(("127.0.0.1", 42))
        self.assertEqual(s.size(), 1)
        s.add(("0.0.0.0", 34))
        self.assertEqual(s.size(), 2)
        s.remove(("0.0.0.0", 34))
        self.assertEqual(s.size(), 1)
        s.add(("0.0.0.0", 34))
        self.assertEqual(s.size(), 2)

if __name__ == '__main__':
    unittest.main()
