"""
Set of active storage servers
"""
from typing import Set, List

from name_server.common import Location
from name_server.storage_server_proxy import StorageServerProxy


class StorageServerSet:
    """
    Set of active storage servers
    """

    storage_servers: List[StorageServerProxy]

    class ServerAlreadyAddedError(RuntimeError):
        """
        Adding a server that is already in the set
        """

    class ServerNotFoundError(RuntimeError):
        """
        No such server found in the set
        """

    storage_servers: Set[Location]

    def __init__(self):
        self.storage_servers = set()

    def add(self, location: Location):
        if self.contains(location):
            raise StorageServerSet.ServerAlreadyAddedError("server already in the set")
        self.storage_servers.add(StorageServerProxy(location))

    def remove(self, location: Location):
        if not self.contains(location):
            raise StorageServerSet.ServerNotFoundError("server not present in the set")
        node = next(i for i in self.storage_servers if i.address == location)
        self.storage_servers.remove(node)

    def contains(self, location: Location) -> bool:
        for server in self.storage_servers:
            if server.address == location:
                return True
        return False

    def get(self, idx: int) -> StorageServerProxy:
        if idx >= self.size() or idx < 0:
            raise IndexError("index out of range")
        iterator = iter(self.storage_servers)
        while idx >= 0:
            elem = next(iterator)
            idx -= 1
        return elem

    def size(self) -> int:
        return len(self.storage_servers)
