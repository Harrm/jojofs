"""
Implements RPC calls to the naming server
"""

from ipaddress import IPv4Address
from xmlrpc.server import SimpleXMLRPCServer

from name_server.common import FileInfo
from name_server.filesystem_meta_storage import FilesystemMetaStorage
from name_server.storage_server_set import StorageServerSet
from name_server.strategy import DistributionStrategy, ReplicationStrategy


class Server:
    """
    Implements RPC calls to the naming server
    """
    rpc_server: SimpleXMLRPCServer
    storage_server_set: StorageServerSet
    metadata_storage: FilesystemMetaStorage

    def __init__(self, distribution_strategy: DistributionStrategy, replication_strategy: ReplicationStrategy,
                 storage_server_set: StorageServerSet):
        self.distribution_strategy = distribution_strategy
        self.replication_strategy = replication_strategy
        self.storage_server_set = storage_server_set

    def start(self, address: IPv4Address, port: int):
        """
        :param address:
        :param port:
        :return:
        """
        self.rpc_server = SimpleXMLRPCServer((address, port))
        self.rpc_server.register_function(self.find_best_storage, "find_storage")

    def invoke_replication(self, file_info: FileInfo):
        """
        :param file_info:
        :return:
        """
        for location in self.replication_strategy.get_replica_set_of(file_info):
            _ = self.storage_server_set.get(location).replicate(file_info, location)

    def ping_storage_servers(self):
        raise NotImplementedError

    def find_best_storage(self, file_info):
        # less loaded storage
        # storage with the containing dir
        return NotImplementedError

    def complete_file_creation(self, file_info):
        # invoke replication
        raise NotImplementedError
