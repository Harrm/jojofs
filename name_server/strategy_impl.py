"""
Implementations of name server strategies
"""
from typing import List
import random

from name_server.common import FileInfo, Location
from name_server.storage_server_set import StorageServerSet
from name_server.strategy import DistributionStrategy, ReplicationStrategy


class UniformDistributionStrategy(DistributionStrategy):
    """
    Picks a random storage with a uniform distribution
    """

    storage_set: StorageServerSet

    def __init__(self, storage_set: StorageServerSet):
        self.storage_set = storage_set

    def get_location_for(self, file_info: FileInfo) -> Location:
        """
        Does not guarantee to return the same value if called with the same file info twice
        """
        i = random.uniform(0, self.storage_set.size())
        return self.storage_set.get(i)


class UniformReplicationStrategy(ReplicationStrategy):
    """
    Picks random storages with a uniform distribution
    """

    class StorageSetTooSmallError(RuntimeError):
        """Storage set is less than replication factor"""

    storage_set: StorageServerSet

    def __init__(self, storage_set: StorageServerSet, replica_factor: int):
        self.storage_set = storage_set
        self.replica_factor = replica_factor

    def get_replica_set_of(self, file_info: FileInfo) -> List[Location]:
        """
        Does not guarantee to return the same value if called with the same file info twice
        """
        if self.storage_set.size() < self.replica_factor:
            raise UniformReplicationStrategy.StorageSetTooSmallError
        return random.sample(self.storage_set, self.replica_factor)
