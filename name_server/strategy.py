"""
Interfaces for abstract server strategies
"""

import abc
from typing import List

from name_server.common import FileInfo, Location


class DistributionStrategy:
    """
    Interface for file distribution strategies, like uniform or DHT-based
    """

    @abc.abstractmethod
    def get_location_for(self, file_info: FileInfo) -> Location:
        """
        Does not guarantee to return the same value if called with the same file info twice
        """


class ReplicationStrategy:
    """
    Interface for file replication strategies, like uniform or DHT-based
    """

    @abc.abstractmethod
    def get_replica_set_of(self, file_info: FileInfo) -> List[Location]:
        """
        Does not guarantee to return the same value if called with the same file info twice
        """
