"""
Storage for the metainformation of the system
"""
from __future__ import annotations

import datetime
from pathlib import Path
from typing import List, Tuple, Optional, Union

from name_server.common import FileInfo, FileAttrs


class _Node:
    """
    A node in the filesystem graph
    """

    info: FileInfo
    parent: Optional[_Node]
    children: List[_Node]

    def __init__(self, info: FileInfo, parent: Optional[_Node]):
        self.info = info
        self.parent = parent
        self.children = []
        if parent is not None:
            parent.children.append(self)

    def get_child(self, name: str) -> Optional[_Node]:
        for child in self.children:
            if child.info.name == name:
                return child
        return None


class FilesystemMetaStorage:
    """
    Storage for the meta information of the  filesystem
    """

    class PathNotAbsoluteError(RuntimeError):
        """
        Provided path must be absolute, but it's not
        """

    class NoSuchDirError(RuntimeError):
        """
        No such directory in the filesystem
        """

    class DirectoryNotEmptyError(RuntimeError):
        """
        Removing a non empty directory
        """

    class NotDirectoryError(RuntimeError):
        """
        Directory-only operation over a non directory file
        """

    class RemovalOfRootError(RuntimeError):
        """
        Removing file system root is not allowed
        """

    root: _Node

    def __init__(self):
        info = FileInfo(id='0', name='/',
                        attrs=FileAttrs(creation_time=datetime.datetime.now(), modification_time=datetime.datetime.now()
                                        , is_dir=True))
        self.root = _Node(info, None)

    def put(self, file_info: FileInfo, path: Path):
        path_valid, node = self._check_if_path_exists(path)
        if not path_valid:
            raise FilesystemMetaStorage.NoSuchDirError(f"A directory {node} does not exist")
        if node.info.attrs.is_dir:
            _ = _Node(info=file_info, parent=node)
        else:
            raise FilesystemMetaStorage.NotDirectoryError("Attempt to a file inside a file, which is not a directory")

    def remove(self, path: Path, *, recursive: bool = False):
        if str(path) == path.root:
            raise FilesystemMetaStorage.RemovalOfRootError("Attempt to remove the root")
        path_valid, node = self._check_if_path_exists(path)
        if not path_valid:
            raise FilesystemMetaStorage.NoSuchDirError(f"A directory {node} does not exist")
        if len(node.children) > 0 and not recursive:
            raise FilesystemMetaStorage.DirectoryNotEmptyError(
                "Attempt to remove a non-empty directory." +
                "If it is the desired behaviour, please set 'recursive' option to True")
        node.parent.children.remove(node)
        node.parent = None

    def _check_if_path_exists(self, path: Path) -> Tuple[bool, Union[str, _Node]]:
        """
        :param path: absolute path
        :return: (True, None) if all dirs between the root and the tail exist and (False, $absent_dir_name) otherwise
        """
        if not path.absolute():
            raise FilesystemMetaStorage.PathNotAbsoluteError

        current_node = self.root
        for part in path.parts[1:]:
            child = current_node.get_child(part)
            if child is None:
                return False, part
            current_node = child
        return True, current_node
