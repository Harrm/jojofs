"""
Common types for naming server module
"""
import datetime
from dataclasses import dataclass
from ipaddress import IPv4Address
from typing import Tuple

Location = Tuple[IPv4Address, int]
NodeId = str
FileId = str


def calc_permissions(user_perm: int, group_perm: int, all_perm: int):
    return user_perm << 6 | group_perm << 3 | all_perm

# permissions
NONE = 0
ALLOW_READ = 0b100
ALLOW_WRITE = 0b010
ALLOW_EXEC = 0b001


@dataclass
class FileAttrs:
    """
    Attributes of a file
    """
    creation_time: datetime.datetime
    modification_time: datetime.datetime
    permissions: int = calc_permissions(ALLOW_READ, NONE, NONE)
    owner: str = 'root'
    group: str = 'root'
    is_dir: bool = False


@dataclass
class FileInfo:
    """
    Info of a file
    """
    id: FileId
    name: str
    attrs: FileAttrs
