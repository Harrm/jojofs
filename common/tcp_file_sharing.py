"""
Utils for sending and receiving files sing tcp
"""
import socket
from threading import Thread
from typing import Iterable, Callable


def _server(host, action: Callable[[socket.socket], None]):
    sock = socket.socket()
    sock.bind((host, 0))
    sock.listen(1)
    port = sock.getsockname()[1]

    def _serve():
        with sock:
            conn, _ = sock.accept()
            with conn:
                action(conn)

    Thread(target=_serve).start()

    return port


def server_downloader(host, consumer: Callable[[bytes], None]):
    def _action(sock: socket.socket):
        while True:
            data = sock.recv(1024)
            if not data:
                break
            consumer(data)

    return _server(host, _action)


def server_uploader(host, payload: Iterable[bytes]):
    def _action(conn):
        for bts in payload:
            conn.send(bts)

    return _server(host, _action)


def client_uploader(host, port, payload: Iterable[bytes]):
    with socket.socket() as sock:
        sock.connect((host, port))
        for bts in payload:
            sock.send(bts)


def client_downloader(host, port, consumer: Callable[[bytes], None]):
    with socket.socket() as sock:
        sock.connect((host, port))
        while True:
            data = sock.recv(1024)
            if not data:
                break
            consumer(data)
